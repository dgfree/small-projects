"""This program finds the coin weights from the user and determines how
many coin rolls they will need for their coins.
"""

from math import ceil

class CoinConstants(object):
    coin_names = ['penny', 'nickel', 'dime', 'quarter', 'half dollar']
    coin_weights = {'penny': 2.5, 'nickel': 5.0, 'dime': 2.268,
                          'quarter': 5.67, 'half dollar': 11.34}
    coin_wrapper_count = {'penny': 50, 'nickel': 40, 'dime': 50,
                          'quarter': 40, 'half dollar': 20}


class UserInput(object):
    """Retrieves user input from the user."""

    def determine_units(self):
        """Determines whether the user wants to use grams or pounds."""
        print """Would you like to input your weights as grams or pounds?
              Enter P for pounds and G for grams.
              """

        valid_choice = False

        while not valid_choice:
            unit_choice = raw_input('Please enter P or G.\n> ')
            # valid inputs are only one character long
            if unit_choice in 'PpGg' and len(unit_choice) == 1:
                valid_choice = True

        if unit_choice in 'Pp':
            unit_choice = 'pounds'
        else:
            unit_choice = 'grams'

        return unit_choice

    def get_weights(self, units):
        coin_weights = {}
        for coin in CoinConstants.coin_names:
            print "Please enter the weight of %s in %s" % (coin, units)
            valid_choice = False
            # integer inputs only
            while not valid_choice:
                try:
                    weight = int(raw_input('> '))
                except:
                    print "Invalid input. Please try again."
                else:
                    valid_choice = True
                    coin_weights[coin] = weight

        return coin_weights

class Conversions(object):
    def grams_to_pounds(self, grams):
        return grams * 0.00220462

    def pounds_to_grams(self, pounds):
        return pounds * 453.592

    def grams_to_wrappers(self, user_coin_weights):
        """Determines the amount of wrappers per coins.

        coin_weights = dictionary of coin names and weights
        """
        user_coin_wrapper = {}
        for coin, weight in user_coin_weights:
            coin_count = ceil(weight / CoinConstants.coin_weights[coin])
            wrappers = ceil(coin_count / coin_wrapper_count[coin])
            user_coin_wrapper[coin] = wrappers

        return user_coin_wrapper

    def pounds_to_wrappers(self, user_coin_weights):
        """Convert to grams, then use grams_to_wrappers"""
        for coin, weight in user_coin_weights:
            pass
